# GitLab Runner
[GitLab yugioh-web-tool](https://gitlab.com/yugioh-web-tools)と連携するKubernetes Executorです。

## GitLab Runnerのデプロイ
デプロイは手動で行ってください

kubernetes runner実行中に自身(gitlab runner)の更新はできなそうなので。

```
TOKEN=(cat token) helmfile sync
kubectl apply -f serviceaccount.yaml
```

## helper image
以下は、kubernetesデプロイ用のイメージです

`registry.gitlab.com/slimemoss/gitlab-runner/deploy`
